package net.wellcore.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import static net.wellcore.util.SubmodeType.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public enum Mode {
    LOGIN((list) -> addSubmode(list, AUTH, "login")),
    HUB((list) -> addSubmode(list, MAIN, "hub")),
    LUCKYWARS((list) -> {
        addSubmode(list, LOBBY, "lwlobby");
        addSubmode(list, GAME, "lws");
        addSubmode(list, GAME, "lwd");
        addSubmode(list, GAME, "lwt");
    }),
    VANILLA((list) -> {
        addSubmode(list, SURVIVAL, "vanilla");
    }),
    ;

    Consumer<List<Submode>> initializer;
    List<Submode> submodes;

    Mode(Consumer<List<Submode>> initializer) {
        this.initializer = initializer;
        submodes = new ArrayList<>();
        initializer.accept(submodes);
    }

    public static void addSubmode(List<Submode> list, SubmodeType type, String prefix) {
        list.add(new Submode(type, prefix));
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class Submode {
        SubmodeType type;
        String prefix;
    }
}
