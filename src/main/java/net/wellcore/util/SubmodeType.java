package net.wellcore.util;

public enum SubmodeType {
    UNKNOWN, GAME, LOBBY, MAIN, AUTH, SURVIVAL, LIMBO
}
