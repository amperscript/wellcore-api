package net.wellcore.util;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.util.HashMap;
import java.util.Map;

public enum GameVersion {

    UNKNOWN(-1, "Unknown"),
    MINECRAFT_1_8(
            47, "1.8",
            "1.8.1-pre1", "1.8.1-pre2", "1.8.1-pre3",
            "1.8.1-pre4", "1.8.1-pre5", "1.8.1", "1.8.2-pre1", "1.8.2-pre1",
            "1.8.2-pre2", "1.8.2-pre3", "1.8.2-pre4", "1.8.2-pre5", "1.8.2-pre6",
            "1.8.2-pre7", "1.8.2", "1.8.3", "1.8.4", "1.8.5", "1.8.6",
            "1.8.7", "1.8.8", "1.8.9"
    ),
    MINECRAFT_15W14A(48, "15w14a"),
    MINECRAFT_15W31A(49, "15w31a"),
    MINECRAFT_15W31B(50, "15w31b"),
    MINECRAFT_15W31C(51, "15w31c"),
    MINECRAFT_15W32A(52, "15w32a"),
    MINECRAFT_15W32B(53, "15w32b"),
    MINECRAFT_15W32C(54, "15w32c"),
    MINECRAFT_15W33A(55, "15w33a"),
    MINECRAFT_15W33B(56, "15w33b"),
    MINECRAFT_15W33C(57, "15w33c"),
    MINECRAFT_15W34A(58, "15w34a"),
    MINECRAFT_15W34B(59, "15w34b"),
    MINECRAFT_15W34C(60, "15w34c"),
    MINECRAFT_15W34D(61, "15w34d"),
    MINECRAFT_15W35A(62, "15w35a"),
    MINECRAFT_15W35B(63, "15w35b"),
    MINECRAFT_15W35C(64, "15w35c"),
    MINECRAFT_15W35D(65, "15w35d"),
    MINECRAFT_15W35E(66, "15w35e"),
    MINECRAFT_15W36A(67, "15w36a"),
    MINECRAFT_15W36B(68, "15w36b"),
    MINECRAFT_15W36C(69, "15w36c"),
    MINECRAFT_15W36D(70, "15w36d"),
    MINECRAFT_15W37A(71, "15w37a"),
    MINECRAFT_15W38A(72, "15w38a"),
    MINECRAFT_15W38B(73, "15w38b"),
    MINECRAFT_15W39A(74, "15w39a", "15w39b", "15w39c"),
    MINECRAFT_15W40A(75, "15w40a"),
    MINECRAFT_15W40B(76, "15w40b"),
    MINECRAFT_15W41A(77, "15w41a"),
    MINECRAFT_15W41B(78, "15w41b"),
    MINECRAFT_15W42A(79, "15w42a"),
    MINECRAFT_15W43A(80, "15w43a"),
    MINECRAFT_15W43B(81, "15w43b"),
    MINECRAFT_15W43C(82, "15w43c"),
    MINECRAFT_15W44A(83, "15w44a"),
    MINECRAFT_15W44B(84, "15w44b"),
    MINECRAFT_15W45A(85, "15w45a"),
    MINECRAFT_15W46A(86, "15w46a"),
    MINECRAFT_15W47A(87, "15w47a"),
    MINECRAFT_15W47B(88, "15w47b"),
    MINECRAFT_15W47C(89, "15w47c"),
    MINECRAFT_15W49A(90, "15w49a"),
    MINECRAFT_15W49B(91, "15w49b"),
    MINECRAFT_15W50A(92, "15w50a"),
    MINECRAFT_15W51A(93, "15w51a"),
    MINECRAFT_15W51B(94, "15w51b"),
    MINECRAFT_16W02A(95, "16w02a"),
    MINECRAFT_16W03A(96, "16w03a"),
    MINECRAFT_16W04A(97, "16w04a"),
    MINECRAFT_16W05A(98, "16w05a"),
    MINECRAFT_16W05B(99, "16w05b"),
    MINECRAFT_16W06A(100, "16w06a"),
    MINECRAFT_16W07A(101, "16w07a"),
    MINECRAFT_16W07B(102, "16w07b"),
    MINECRAFT_1_9_PRE_1(103, "1.9-pre1"),
    MINECRAFT_1_9_PRE_2(104, "1.9-pre2"),
    MINECRAFT_1_9_PRE_3(105, "1.9-pre3"),
    MINECRAFT_1_9_PRE_4(106, "1.9-pre4"),
    MINECRAFT_1_9(107, "1.9", "1.9.1-pre1"),
    MINECRAFT_1_9_1(108, "1.9.1", "1.9.1-pre2", "1.9.1-pre3", "1.RV-Pre1"),
    MINECRAFT_1_9_2(109, "1.9.2", "16w14a", "16w15a", "16w16b", "1.9.3-pre1"),
    MINECRAFT_1_9_3(110, "1.9.3", "1.9.3-pre2", "1.9.3-pre3", "1.9.4"),
    MINECRAFT_16W20A(201, "16w20a"),
    MINECRAFT_16W21A(202, "16w21a"),
    MINECRAFT_16W21B(203, "16w21b"),
    MINECRAFT_1_10_PRE_1(204, "1.10-pre1"),
    MINECRAFT_1_10_PRE_2(205, "1.10-pre2"),
    MINECRAFT_1_10(210, "1.10", "1.10.1", "1.10.2"),
    MINECRAFT_16W32A(301, "16w32a"),
    MINECRAFT_16W32B(302, "16w32b"),
    MINECRAFT_16W33A(303, "16w33a"),
    MINECRAFT_16W35A(304, "16w35a"),
    MINECRAFT_16W36A(306, "16w36a"),
    MINECRAFT_16W38A(307, "16w38a"),
    MINECRAFT_16W39A(308, "16w39a"),
    MINECRAFT_16W39B(310, "16w39b"),
    MINECRAFT_16W39C(311, "16w39c"),
    MINECRAFT_16W40A(312, "16w40a"),
    MINECRAFT_16W41A(313, "16w41a"),
    MINECRAFT_16W42A(314, "16w42a"),
    MINECRAFT_16W43A(313, "16w43a", "16w44a"),
    MINECRAFT_1_11(315, "1.11"),
    MINECRAFT_1_11_1(316, "1.11.1", "1.11.2", "16w50a"),
    MINECRAFT_17W06A(317, "17w06a"),
    MINECRAFT_17W13A(318, "17w13a"),
    MINECRAFT_17W13B(319, "17w13b"),
    MINECRAFT_17W14A(320, "17w14a"),
    MINECRAFT_17W15A(321, "17w15a"),
    MINECRAFT_17W16A(322, "17w16a"),
    MINECRAFT_17W16B(323, "17w16b"),
    MINECRAFT_17W17A(324, "17w17a"),
    MINECRAFT_17W17B(325, "17w17b"),
    MINECRAFT_17W18A(326, "17w18a"),
    MINECRAFT_17W18B(327, "17w18b"),
    MINECRAFT_1_12_PRE1(328, "1.12-pre1"),
    MINECRAFT_1_12_PRE2(329, "1.12-pre2"),
    MINECRAFT_1_12_PRE3(330, "1.12-pre3"),
    MINECRAFT_1_12_PRE4(331, "1.12-pre4"),
    MINECRAFT_1_12_PRE5(332, "1.12-pre5"),
    MINECRAFT_1_12_PRE6(333, "1.12-pre6"),
    MINECRAFT_1_12_PRE7(334, "1.12-pre7"),
    MINECRAFT_1_12(335, "1.12"),
    MINECRAFT_17W31A(336, "17w31a"),
    MINECRAFT_1_12_1_PRE1(337, "1.12.1-pre1"),
    MINECRAFT_1_12_1(338, "1.12.1"),
    MINECRAFT_1_12_2_PRE1(339, "1.12.2-pre1", "1.12.2-pre2"),
    MINECRAFT_1_12_2(340, "1.12.2"),
    MINECRAFT_17W43A(341, "17w43a"),
    MINECRAFT_17W43B(342, "17w43b"),
    MINECRAFT_17W45A(343, "17w45a"),
    MINECRAFT_17W45B(344, "17w45b"),
    MINECRAFT_17W46A(345, "17w46a"),
    MINECRAFT_17W47A(346, "17w47a"),
    MINECRAFT_17W47B(347, "17w47b"),
    MINECRAFT_17W48A(348, "17w48a"),
    MINECRAFT_17W49A(349, "17w49a"),
    MINECRAFT_17W49B(350, "17w49b"),
    MINECRAFT_17W50A(351, "17w50a"),
    MINECRAFT_18W01A(352, "18w01a"),
    MINECRAFT_18W02A(353, "18w02a"),
    MINECRAFT_18W03A(354, "18w03a"),
    MINECRAFT_18W03B(355, "18w03b"),
    MINECRAFT_18W05A(356, "18w05a"),
    MINECRAFT_18W06A(357, "18w06a"),
    MINECRAFT_18W07A(358, "18w07a"),
    MINECRAFT_18W07B(359, "18w07b"),
    MINECRAFT_18W07C(360, "18w07c"),
    MINECRAFT_18W08A(361, "18w08a"),
    MINECRAFT_18W08B(362, "18w08b"),
    MINECRAFT_18W09A(363, "18w09a"),
    MINECRAFT_18W10A(364, "18w10a"),
    MINECRAFT_18W10B(365, "18w10b"),
    MINECRAFT_18W10C(366, "18w10c"),
    MINECRAFT_18W10D(367, "18w10d"),
    MINECRAFT_18W11A(368, "18w11a"),
    MINECRAFT_18W14A(369, "18w14a"),
    MINECRAFT_18W14B(370, "18w14b"),
    MINECRAFT_18W15A(371, "18w15a"),
    MINECRAFT_18W16A(372, "18w16a"),
    MINECRAFT_18W19A(373, "18w19a"),
    MINECRAFT_18W19B(374, "18w19b"),
    MINECRAFT_18W20A(375, "18w20a"),
    MINECRAFT_18W20B(376, "18w20b"),
    MINECRAFT_18W20C(377, "18w20c"),
    MINECRAFT_18W21A(378, "18w21a"),
    MINECRAFT_18W21B(379, "18w21b"),
    MINECRAFT_18W22A(380, "18w22a"),
    MINECRAFT_18W22B(381, "18w22b"),
    MINECRAFT_18W22C(382, "18w22c"),
    MINECRAFT_1_13_PRE1(383, "1.13-pre1"),
    MINECRAFT_1_13_PRE2(384, "1.13-pre2"),
    MINECRAFT_1_13_PRE3(385, "1.13-pre3"),
    MINECRAFT_1_13_PRE4(386, "1.13-pre4"),
    MINECRAFT_1_13_PRE5(387, "1.13-pre5"),
    MINECRAFT_1_13_PRE6(388, "1.13-pre6"),
    MINECRAFT_1_13_PRE7(389, "1.13-pre7"),
    MINECRAFT_1_13_PRE8(390, "1.13-pre8"),
    MINECRAFT_1_13_PRE9(391, "1.13-pre9"),
    MINECRAFT_1_13_PRE10(392, "1.13-pre10"),
    MINECRAFT_1_13(393, "1.13"),
    MINECRAFT_18W30A(394, "18w30a"),
    MINECRAFT_18W30B(395, "18w30b"),
    MINECRAFT_18W31A(396, "18w31a"),
    MINECRAFT_18W32A(397, "18w32a"),
    MINECRAFT_18W33A(398, "18w33a"),
    MINECRAFT_1_13_1_PRE1(399, "1.13.1-pre1"),
    MINECRAFT_1_13_1_PRE2(400, "1.13.1-pre2"),
    MINECRAFT_1_13_1(401, "1.13.1"),
    MINECRAFT_1_13_2_PRE1(402, "1.13.1-pre1"),
    MINECRAFT_1_13_2_PRE2(403, "1.13.1-pre2"),
    MINECRAFT_1_13_2(404, "1.13.2"),
    MINECRAFT_18W43A(441, "18w43a", "18w43b"),
    MINECRAFT_18W43C(442, "18w43c"),
    MINECRAFT_18W44A(443, "18w44a"),
    MINECRAFT_18W45A(444, "18w45a"),
    MINECRAFT_18W46A(445, "18w46a"),
    MINECRAFT_18W47A(446, "18w47a"),
    MINECRAFT_18W47B(447, "18w47b"),
    MINECRAFT_18W48A(448, "18w48a"),
    MINECRAFT_18W48B(449, "18w48b"),
    MINECRAFT_18W49A(450, "18w49a"),
    MINECRAFT_18W50A(451, "18w50a"),
    MINECRAFT_19W02A(452, "19w02a"),
    MINECRAFT_19W03A(453, "19w03a"),
    MINECRAFT_19W03B(454, "19w03b"),
    MINECRAFT_19W03C(455, "19w03c"),
    MINECRAFT_19W04A(456, "19w04a"),
    MINECRAFT_19W04B(457, "19w04b"),
    MINECRAFT_19W05A(458, "19w05a"),
    MINECRAFT_19W06A(459, "19w06a"),
    MINECRAFT_19W07A(460, "19w07a"),
    MINECRAFT_19W08A(461, "19w08a"),
    MINECRAFT_19W08B(462, "19w08b"),
    MINECRAFT_19W09A(463, "19w09a"),
    MINECRAFT_19W11A(464, "19w11a"),
    MINECRAFT_19W11B(465, "19w11b"),
    MINECRAFT_19W12A(466, "19w12a"),
    MINECRAFT_19W12B(467, "19w12b"),
    MINECRAFT_19W13A(468, "19w13a"),
    MINECRAFT_19W13B(469, "19w13b"),
    MINECRAFT_19W14A(470, "19w14a"),
    MINECRAFT_19W14B(471, "19w14b"),
    MINECRAFT_1_14_PRE1(472, "1.14-pre1"),
    MINECRAFT_1_14_PRE2(473, "1.14-pre2"),
    MINECRAFT_1_14_PRE3(474, "1.14-pre3"),
    MINECRAFT_1_14_PRE4(475, "1.14-pre4"),
    MINECRAFT_1_14_PRE5(476, "1.14-pre5"),
    MINECRAFT_1_14(477, "1.14"),
    MINECRAFT_1_14_1_PRE1(478, "1.14.1-pre1"),
    MINECRAFT_1_14_1_PRE2(479, "1.14.1-pre2"),
    MINECRAFT_1_14_1(480, "1.14.1"),
    MINECRAFT_1_14_2_PRE1(481, "1.14.2-pre1"),
    MINECRAFT_1_14_2_PRE2(482, "1.14.2-pre2"),
    MINECRAFT_1_14_2_PRE3(483, "1.14.2-pre3"),
    MINECRAFT_1_14_2_PRE4(484, "1.14.2-pre4"),
    MINECRAFT_1_14_2(485, "1.14.2"),
    MINECRAFT_1_14_3_PRE1(486, "1.14.3-pre1"),
    MINECRAFT_1_14_3_PRE2(487, "1.14.3-pre2"),
    MINECRAFT_1_14_3_PRE3(488, "1.14.3-pre3"),
    MINECRAFT_1_14_3_PRE4(489, "1.14.3-pre4"),
    MINECRAFT_1_14_3_CT(500, "1.14.3 combat test"),
    MINECRAFT_1_14_3(490, "1.14.3"),
    MINECRAFT_1_14_4_PRE1(491, "1.14.4-pre1"),
    MINECRAFT_1_14_4_PRE2(492, "1.14.4-pre2"),
    MINECRAFT_1_14_4_PRE3(493, "1.14.4-pre3"),
    MINECRAFT_1_14_4_PRE4(494, "1.14.4-pre4"),
    MINECRAFT_1_14_4_PRE5(495, "1.14.4-pre5"),
    MINECRAFT_1_14_4_PRE6(496, "1.14.4-pre6"),
    MINECRAFT_1_14_4_PRE7(497, "1.14.4-pre7"),
    MINECRAFT_1_14_4(498, "1.14.4"),
    MINECRAFT_1_15(573, "1.15"),
    MINECRAFT_1_15_1(575, "1.15.1"),
    MINECRAFT_1_15_2(578, "1.15.2"),
    MINECRAFT_1_16(735, "1.16"),
    MINECRAFT_1_16_1(736, "1.16.1"),
    MINECRAFT_1_16_2(751, "1.16.2"),
    MINECRAFT_1_16_3(753, "1.16.3"),
    MINECRAFT_1_16_4(754, "1.16.4"),
    MINECRAFT_1_17(755, "1.17"),
    MINECRAFT_1_17_1(756, "1.17.1"),
    MINECRAFT_1_18(757, "1.18");

    private static final TIntObjectMap<GameVersion> BY_PROTOCOL_NUMBER = new TIntObjectHashMap<>();
    private static final Map<String, GameVersion> BY_NAME = new HashMap<>();

    static {
        for (GameVersion version : values()) {
            BY_PROTOCOL_NUMBER.put(version.getVersion(), version);

            for (String name : version.names) {
                BY_NAME.put(name.toLowerCase(), version);
            }
        }
    }

    private final int version;
    private final String[] names;

    private GameVersion(int version, String... names) {
        this.version = version;
        this.names = names;
    }

    public int getVersion() {
        return version;
    }

    public String getName() {
        return names[0];
    }

    @Override
    public String toString() {
        return getName();
    }

    public static GameVersion getByProtocolNumber(int id) {
        GameVersion version = BY_PROTOCOL_NUMBER.get(id);

        if (version == null) {
            return UNKNOWN;
        }

        return version;
    }

    public static GameVersion getByName(String name) {
        return BY_NAME.getOrDefault(name.toLowerCase(), UNKNOWN);
    }


}