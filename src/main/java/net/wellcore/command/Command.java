package net.wellcore.command;

import lombok.NonNull;
import net.wellcore.sender.WellSender;

public interface Command<T extends WellSender> {

    void execute(@NonNull T sender, String[] args);
}
