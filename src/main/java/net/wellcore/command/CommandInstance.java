package net.wellcore.command;

public @interface CommandInstance {
    String[] value();
}
