package net.wellcore.command;

import net.wellcore.donate.Group;

public @interface CommandGroup {
    Group value();
}
