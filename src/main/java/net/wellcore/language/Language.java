package net.wellcore.language;

import java.util.List;

public interface Language {

    /**
     * Get Language name e.g.: "Russian"
     */
    String getName();

    /**
     * Get language identifier e.g.: "ru"
     */
    String getIdentifier();

    /**
     * Get language translators
     */
    List<String> getAuthors();

    /**
     * Get a message on his key with replayed placeholders.
     *
     * @param key - language key
     * @param replacement - placeholders
     * @return - final message (to send)
     */
    String get(String key, Object... replacement);
}
