package net.wellcore;

import net.wellcore.command.Command;
import net.wellcore.donate.Group;
import net.wellcore.language.Language;
import net.wellcore.server.ServerService;
import net.wellcore.user.UserService;

import java.util.Collection;

public interface WellCore {

    UserService getUserService();
    ServerService getServerService();
    void registerCommand(Class<? extends Command> commandClass);

    Language getLanguage(String identifier);
    Collection<Language> getLanguages();


}
