package net.wellcore.user;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface UserService {

    /**
     * Get the user by their playerName.
     *
     * @param name - playerName.
     * @return - user.
     */
    CompletableFuture<User> getUser(String name);

    /**
     * Get the user by their UUID.
     *
     * @param uuid - playerName.
     * @return - user.
     */
    CompletableFuture<User> getUser(UUID uuid);
}
