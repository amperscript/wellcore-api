package net.wellcore.user;

import lombok.NonNull;
import net.wellcore.donate.Group;
import net.wellcore.language.Language;
import net.wellcore.util.GameVersion;

import java.net.InetSocketAddress;
import java.time.ZonedDateTime;
import java.util.UUID;

public interface User {

    /**
     * Group's section;
     */
    Group getGroup();
    void setGroup(Group groupLevel);
    boolean hasGroup(Group groupLevel);

    /**
     * Language's section;
     */
    Language getLanguage();
    void setLanguage(Language language);

    /**
     * Subscription's section;
     */
    boolean hasSubscription();
    ZonedDateTime getSubscriptionEnd();

    /**
     * Server's section;
     */
    String getName();
    String getServer();
    String getProxy();

    ZonedDateTime getLatestOnline();
    ZonedDateTime getRegisterDate();
    UUID getUUID();
    InetSocketAddress getAddress();
    GameVersion getVersion();
    void redirect(@NonNull String server);

    /**
     * Property's section;
     */
    int getProperty(String propertyName);
    void setProperty(String propertyName, int value);
    default void addProperty(String propertyName, int value) {
        setProperty(propertyName, getProperty(propertyName) + value);
    }
    default void takeProperty(String propertyName, int value) {
        setProperty(propertyName, getProperty(propertyName) - value);
    }

    /**
     * Prefix's section;
     */
    String getPrefix();
    void setPrefix(String prefix);

    /**
     * Coin's section;
     */
    default int getCoins() {
        return getProperty("COINS");
    }

    default void setCoins(int coins) {
        setProperty("COINS", coins);
    }

    default void addCoins(int coins) {
        addProperty("COINS", coins);
    }

    default void takeCoins(int coins) {
        takeProperty("COINS", coins);
    }

    /**
     * Dust's section;
     */
    default int getDusts() {
        return getProperty("DUSTS");
    }

    default void setDusts(int dusts) {
        setProperty("DUSTS", dusts);
    }

    default void addDusts(int dusts) {
        addProperty("DUSTS", dusts);
    }

    default void takeDusts(int dusts) {
        takeProperty("DUSTS", dusts);
    }

    /**
     * Suffix's section;
     */
    String getSuffix();
    void setSuffix(String prefix);

}
