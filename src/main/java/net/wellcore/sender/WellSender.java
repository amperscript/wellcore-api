package net.wellcore.sender;

import lombok.NonNull;

public interface WellSender {
    void sendMessage(@NonNull String message);
    void sendLangMessage(@NonNull String message, Object... replacement);
    boolean isPlayer();
    boolean isOnline();
}
