package net.wellcore.server;

import java.net.InetSocketAddress;
import java.util.Collection;

public interface Proxy {
    String getName();
    Collection<String> getPlayers();
    String getNode();
    InetSocketAddress getAddress();
}
