package net.wellcore.server;

public enum State {

    WHITELIST(),
    WAITING(),
    STARTING(),
    GAME(),
    FULL(),
    END();

}

