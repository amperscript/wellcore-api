package net.wellcore.server;

import lombok.NonNull;
import net.wellcore.util.Mode;

import java.util.concurrent.CompletableFuture;

public interface ServerService {
    CompletableFuture<Server> getServer(@NonNull String name);
    CompletableFuture<Server> getBestServer(@NonNull Mode.Submode submode);
    CompletableFuture<Server> getBestGameServer(@NonNull Mode.Submode submode);
    CompletableFuture<Proxy> getProxy(@NonNull String name);

}
