package net.wellcore.server;

import java.net.InetSocketAddress;
import java.util.Collection;

public interface Server {
    String getName();
    Collection<String> getPlayers();
    Collection<String> getGamers();
    Collection<String> getSpectators();
    int getMaxPlayers();
    State getState();
    InetSocketAddress getAddress();
    String getNode();

}
