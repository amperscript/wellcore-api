package net.wellcore.donate;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
@Getter
public enum Group {

    DEFAULT(0, '7', 'Y', "§7", "§7"),

    CRYSTAL(10, 'b', 'Q', "§b§lCRYSTAL §b", "§7"),
    ELYTRA(15, 'e', 'P', "§e§lELYTRA §e", "§7"),
    DRAGON(20, '5', 'O', "§5§lDRAGON §5", "§7"),
    ENDER(30, 'd', 'M', "§d§lENDER §d", "§7"),
    ENDER_PLUS(35, '7', 'L', "§7§lENDER+ §7", "§f"),

    MEDIA(61, 'c', 'K', "§c§lMEDIA §c", "§f"),

    TESTER(60, 'f', 'J', "§f§lTESTER §f", "§f"),
    BUILDER(62, '3', 'I', "§3§lBUILDER §3", "§f"),

    WARDEN(70, 'a', 'H', "§a§lWARDEN §a", "§f"),
    HELPER(71, '2', 'G', "§2§lHELPER §2", "§f"),
    MODER(72, '9', 'F',  "§9§lMODER §9", "§f"),


    DEV(100, '3', 'C',  "§3§lDEV §3", "§f"),
    ADMIN(110, '4',  'B', "§4§lADMIN §4", "§f"),
    WITHOUT(115, '5', 'A', "§5§lW/O §5", "§f"),

    ;

    public static final Group[] GROUPS_ARRAY = values();

    private final int level;

    private final char color;

    private final char tagPriority;

    private final String prefix;
    private final String suffix;

    public static Group fromLevel(int level) {
        return Arrays.stream(GROUPS_ARRAY).filter(group -> group.getLevel() == level)
                .findFirst().orElse(null);
    }

    public static Group fromName(String groupName) {
        return Arrays.stream(GROUPS_ARRAY).filter(group -> group.name().equalsIgnoreCase(groupName))
                .findFirst().orElse(null);
    }
}
