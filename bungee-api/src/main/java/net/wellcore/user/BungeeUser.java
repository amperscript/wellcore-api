package net.wellcore.user;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.wellcore.sender.WellSender;

public interface BungeeUser extends User, WellSender {
    ProxiedPlayer toBungee();
}
