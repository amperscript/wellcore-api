package net.wellcore;

import net.wellcore.user.BungeeUserService;

public interface WellBungee extends WellCore {
    @Override
    BungeeUserService getUserService();
}
