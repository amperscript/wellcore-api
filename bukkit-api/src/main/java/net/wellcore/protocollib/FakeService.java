package net.wellcore.protocollib;

import lombok.NonNull;
import net.wellcore.protocollib.holographic.Hologram;
import net.wellcore.protocollib.sidebar.Sidebar;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

public interface FakeService {
    Hologram createHolographic(@NonNull Location location);
    Sidebar createSidebar(@NonNull String title);

    <T extends Entity> FakeEntity<T> summonFakeEntity(@NonNull Class<T> wannableEntity, @NonNull Location location);
}
