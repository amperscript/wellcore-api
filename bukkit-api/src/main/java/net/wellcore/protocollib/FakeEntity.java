package net.wellcore.protocollib;

import org.bukkit.entity.Player;

public interface FakeEntity<T> {
    void show(Player player);
    void hide(Player player);
    void spawn();
    T handle();

}
