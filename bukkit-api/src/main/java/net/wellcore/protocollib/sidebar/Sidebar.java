package net.wellcore.protocollib.sidebar;

import com.google.common.base.Function;
import lombok.NonNull;
import net.wellcore.user.BukkitUser;
import org.bukkit.entity.Player;

public interface Sidebar {
    Sidebar addLine(@NonNull Function<BukkitUser, String> u);
    Sidebar addBlancLine();
    Sidebar addDateLine();

    void show(Player player);
    void hide(Player player);
    void applyToAll();
}
