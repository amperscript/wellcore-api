package net.wellcore.protocollib.holographic;

import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface Hologram {

    Hologram addLangLine(@NonNull String key);
    Hologram addLine(@NonNull String text);
    Hologram addLine(@NonNull ItemStack item);
    Hologram addBlancLine();

    void show(Player player);
    void hide(Player player);
    void spawn();
}
