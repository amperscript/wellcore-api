package net.wellcore.gui;

import lombok.NonNull;
import net.wellcore.user.BukkitUser;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.BiConsumer;

public interface ScrollGui extends Gui {
    void setupMarkup(int... slots);
    void addItem(@NonNull ItemStack item, BiConsumer<InventoryClickEvent, BukkitUser> action);
    void addItem(@NonNull ItemStack item);

}
