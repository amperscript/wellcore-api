package net.wellcore.gui;

import lombok.NonNull;
import net.wellcore.user.BukkitUser;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface Gui {

    void setItem(int slot, @NonNull ItemStack item);
    void setItem(int slot, @NonNull ItemStack item, BiConsumer<InventoryClickEvent, BukkitUser> action);
    boolean hasItem(int slot);
    void open(@NonNull Player player);
    void draw(@NonNull Player player);

}
