package net.wellcore;

import net.wellcore.protocollib.FakeService;
import net.wellcore.user.BukkitUserService;
import net.wellcore.user.UserService;
import net.wellcore.util.Hotbar;

public interface WellBukkit extends WellCore {
    FakeService getFakeService();
    Hotbar createHotbar();
    @Override
    BukkitUserService getUserService();
}
