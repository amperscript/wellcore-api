package net.wellcore.user;

import lombok.NonNull;
import net.wellcore.sender.WellSender;
import org.bukkit.entity.Player;

public interface BukkitUser extends User, WellSender {
    Player toBukkit();
}
