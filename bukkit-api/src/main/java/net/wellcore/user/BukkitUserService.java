package net.wellcore.user;

import lombok.NonNull;
import org.bukkit.entity.Player;

public interface BukkitUserService extends UserService {
    void getUser(@NonNull Player player);
}
