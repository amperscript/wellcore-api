package net.wellcore.util;

import lombok.NonNull;
import net.wellcore.user.BukkitUser;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import java.util.function.Consumer;

public interface Hotbar {
    Hotbar setItem(int slot, @NonNull ItemStack item);
    Hotbar setItem(int slot, @NonNull ItemStack item, Consumer<BukkitUser> action);
    void apply();
    void apply(@NonNull Player player);
}
