package net.test;

import net.wellcore.WellBukkit;
import net.wellcore.protocollib.FakeEntity;
import net.wellcore.protocollib.FakeService;
import org.bukkit.Location;
import org.bukkit.entity.Creeper;

public class WellTest {

    public void summonCreeper(Location location) {
        WellBukkit wellcore = null;
        FakeService service = wellcore.getFakeService();

        FakeEntity<Creeper> fakeCreeper = service.summonFakeEntity(Creeper.class, location);

        fakeCreeper.handle().setPowered(true);
        fakeCreeper.spawn();

        service.createHolographic(location)
                .addLine("здесь был крипер.")
                .spawn();

        service.createSidebar("WELLCORE")
                .addBlancLine()
                .addLine(user -> "Монет: " + user.getProperty("COINS"))
                .addBlancLine()
                .addDateLine()
                .applyToAll();
    }
}
